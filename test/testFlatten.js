const nestedArray = require('../arrayList/nestedArray.js');
const flatten = require('../flatten.js');

const result = flatten(nestedArray);
// console.log(result);

//check expected output

const expectedResult = [1, 2, 3, 4];
if (JSON.stringify(result) === JSON.stringify(expectedResult)) {
    console.log(result);
} else {
    console.log('wrong');
}
