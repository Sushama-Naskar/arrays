const items = require('../arrayList/items.js');
const find = require('../find.js');
function cb(value, index) {
    if (value > 3) {
        return true;
    } else {
        return false;
    }
}
const result = find(items, cb);
// console.log(result);

//check expected output

const expectedResult = items.find(function (value) {
    if (value > 3) {
        return true;
    } else {
        return false;
    }
});

if (result === expectedResult) {
    console.log(result);
} else {
    console.log('wrong');
}
