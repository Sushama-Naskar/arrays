const items = require('../arrayList/items.js');
const map = require('../map.js');

function cb(value, index) {
    return (value * 10);
}
const result = map(items, cb);
// console.log(result);

//expected output
const expectedResult = items.map(function (value) {
    return (value * 10);
})

if (JSON.stringify(result) === JSON.stringify(expectedResult)) {
    console.log(result);
} else {
    console.log('wrong');
}
