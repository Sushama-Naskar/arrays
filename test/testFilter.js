const items = require('../arrayList/items.js');
const filter = require('../filter.js');
function cb(value, index) {
    if (value > 3) {
        return value;
    }
}
const result = filter(items, cb);
// console.log(result);

//check expected output

let expectedResult=items.filter(function(value){
    if(value>3){
        return value;
    }
});

if(JSON.stringify(result)===JSON.stringify(expectedResult)){
    console.log(result);
}else{
    console.log('wrong');
}
