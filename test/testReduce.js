const items = require('../arrayList/items.js');
const reduce = require('../reduce.js');

function cb(startingValue, currentValue) {
    return startingValue + currentValue;
}
const result = reduce(items, cb, 3);

// console.log(result);

//check expected output

let expectedResult = items.reduce(function (startingValue, currentValue) {

    return startingValue + currentValue;

}, 3);

if (result === expectedResult) {
    console.log(result);
} else {
    console.log('wrong');
}
