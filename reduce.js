function reduce(elements, cb, startingValue) {
    if(elements.length==0 && startingValue==undefined)
   throw new Error("Reduce of empty array with no initial value") ;
    let startingIndex = 0;
    if (startingValue == undefined) {
        startingValue = elements[0];
        startingIndex = 1;
    }
    let sum = startingValue;
    for (let index = startingIndex; index < elements.length; index++) {
        sum = cb(sum, elements[index]);
    }
    return sum;
}
module.exports = reduce;
