function flatten(elements) {
    let result = new Array();
    for (let index = 0; index < elements.length; index++) {
        if (Array.isArray(elements[index])) {
            let flatval=flatten(elements[index])
            result = result.concat(flatval);
        } else {
            result.push(elements[index]);
        }

    }
    return result;

}
module.exports = flatten;
