function filter(elements, cb) {
    let result = new Array();
    for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index], index)) {
            result.push(elements[index]);
        }
    }
    return result;
}
module.exports = filter;